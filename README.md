# Quasar App (aspire)

<p>A [Quasar JS](https://quasar.dev/) demo project to demonstrate a money lending app.</p>
<ul>
<b>Note :</b>
<li>The homepage and card page are the same. They do not use any logic from the code</li>
<li><b>Credit page:</b> User can apply for a loan through this page, furnishing information like the loan amount and purpose</li>
<li><b>Payment page:</b> User can payback the loan through the form provided on this page. Three radio buttons are available to choose payment tenure from. It automatically devides the remaining amount into weekly payment mode over 3, 6 or 12 months</li>
<li><b>Settings page:</b> This can be considered as the admin login for the time being. Credit requests can be approved/rejected from this page, the result of which gets reflected in the Loan balance indicator on <b>Credit/Payment</b> page</li>
<li>The webapp uses a firebase app's firestore for the backend</li>
</ul>

<p>The webapp is mobile responsive upto <b>320px</b> screen width.</p>
<p>Click [here](https://aspire-sgp.netlify.app/) to check out the webapp hosted on netlify servers</p>

PS: Hold on for 1-2 seconds for the fresh information to appear after making transactions on <b>Credit/Payments/Settings page</b>. Follow the progress spinner.

**NOTE**: NODE_VERSION is set to 12.22.1 in Netlify to support Quasar

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://v2.quasar.dev/quasar-cli/quasar-conf-js).
