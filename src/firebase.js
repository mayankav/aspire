/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import firebase from 'firebase';

import { ref, onUnmounted } from 'vue';

const firebaseConfig = {
    apiKey: 'AIzaSyDjDx-z1ICjrGS7Znh5jRjmSdEW89iHQZ4',
    authDomain: 'aspire-app-44b15.firebaseapp.com',
    projectId: 'aspire-app-44b15',
    storageBucket: 'aspire-app-44b15.appspot.com',
    messagingSenderId: '1091756022989',
    appId: '1:1091756022989:web:43e85993e2094e038e5d84',
    measurementId: 'G-B1E5H8DTFZ'
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const usersCollection = db.collection('users');
const requestsCollection = db.collection('requests');
const transactionsCollection = db.collection('transactions');

// users
export const createUser = function(user) {
    return usersCollection.add(user);
}

// also used in App.vue for initial vuex store load
export const getUser = async function(id) {
    const user = await usersCollection.doc(id).get();
    return user.exists ? {...user.data(), userId: id} : null;
}

export function updateUser(userId, updateObj) {
    return usersCollection.doc(userId).update(updateObj);
}

// requests
export const createRequest = function(req) {
    // returns a success object that has .id property to be set in the local state
    return requestsCollection.add(req);
}

export function updateRequest(reqId, updateObj) {
    return requestsCollection.doc(reqId).update(updateObj);
}

// used in App.vue for initial vuex store load
export async function getRequests() {
    const snapshot = await requestsCollection.get();
    const reqIds = [];
    snapshot.docs.forEach(doc => reqIds.push(doc.id));
    const allRequestsData = snapshot.docs.map(doc => doc.data());
    for(let i=0; i<reqIds.length; i++) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        allRequestsData[i].requestId = reqIds[i];
    }
    return allRequestsData;
}


// transactions
export const createTransaction = function(tr) {
    // returns a success object that has .id property to be set in the local state
    return transactionsCollection.add(tr);
}

// used in App.vue for initial vuex store load
export async function getTransactions() {
    const snapshot = await transactionsCollection.get();
    const trIds = [];
    snapshot.docs.forEach(doc => trIds.push(doc.id));
    const allTransactionsData = snapshot.docs.map(doc => doc.data());
    for(let i=0; i<trIds.length; i++) {
        allTransactionsData[i].transactionId = trIds[i];
    }
    return allTransactionsData;
}

export function useLoadRequests() {
    const requests = ref([]);
    const cleanupFunction = requestsCollection.onSnapshot(snapshot => {
        requests.value = snapshot.docs.map(doc => doc.data());
    });
    onUnmounted(cleanupFunction);
    return requests;
}
