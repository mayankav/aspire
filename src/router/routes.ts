import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  // embedded in <router-view> in App.vue
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    // embedded in <router-view> in MainLayout.vue
    children: [
      { path: '', component: () => import('pages/Cards.vue') }, 
      { path: '/cards', component: () => import('pages/Cards.vue') },
      { path: '/credit', component: () => import('pages/Credit.vue') },
      { path: '/settings', component: () => import('pages/Settings.vue') },
      { path: '/payments', component: () => import('pages/Payments.vue') }
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
