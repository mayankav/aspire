export interface Todo {
  id: number;
  content: string;
}

export interface Meta {
  totalCount: number;
}

export interface CardDetails {
  name: string;
  valid: Date;
  number: number;
  cvv: number;
}
