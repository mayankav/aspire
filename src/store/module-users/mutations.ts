import { MutationTree } from 'vuex';
import { UserStateInterface } from './state';

const mutations : MutationTree<UserStateInterface> = {
    setUser(state, payload: UserStateInterface) {
        state.userId = payload.userId;
        state.balance = payload.balance;
    },
    updateUserBalance(state, payload: number) {
        state.balance += payload;
    }
}

export default mutations;