export interface UserStateInterface {
    userId: string;
    balance: number;
}

const state: UserStateInterface = {
    userId: '',
    balance: 0
}

export default state;