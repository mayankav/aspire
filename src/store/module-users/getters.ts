import { GetterTree } from 'vuex';
import { UserStateInterface } from './state';
import { StateInterface } from '../index';

const getters  : GetterTree<UserStateInterface, StateInterface> = {
    getUserBalance(state) {
        return state.balance;
    }
}

export default getters;