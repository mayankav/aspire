import state from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

import { Module } from 'vuex';
import { UserStateInterface } from './state';
import { StateInterface } from '../index';

const usersModule: Module<UserStateInterface,StateInterface> = {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}

export default usersModule;