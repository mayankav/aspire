import { ActionTree } from 'vuex';
import { UserStateInterface } from './state';
import { StateInterface } from '../index';

import { updateUser } from '../../firebase';

const actions: ActionTree<UserStateInterface, StateInterface> = {
    setUser(context, payload: UserStateInterface) {
        context.commit('setUser', payload);
    },
    async updateUserBalance(context, payload: number) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        const currentBalance = context.getters.getUserBalance as number;
        await updateUser('u1', {balance: currentBalance+payload});
        context.commit('updateUserBalance', payload); 
    }
}

export default actions;