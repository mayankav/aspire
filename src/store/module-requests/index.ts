import state from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

import { Module } from 'vuex';
import { RequestStateInterface } from './state';
import { StateInterface } from '../index';

const requestsModule: Module<RequestStateInterface,StateInterface> = {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}

export default requestsModule;