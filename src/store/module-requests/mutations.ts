import { MutationTree } from 'vuex';
import { RequestStateInterface, RequestInterface } from './state';

const mutations : MutationTree<RequestStateInterface> = {
    // on first time app load
    setRequests(state, payload: RequestStateInterface) {
        // careful with arrays
        state.requests = [...payload.requests];
    },
    createARequest(state, payload) {
        state.requests.push(payload);
    },
    approveARequest(state, payload: {requestId: string, approvalStatus: RequestInterface['status']}) {
        // the following code triggers vue reactivity 
        state.requests.forEach(request => {
            if(request.requestId == payload.requestId) {
                request.status = payload.approvalStatus;
            }
        })
    }

}

export default mutations;