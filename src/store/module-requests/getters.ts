import { GetterTree } from 'vuex';
import { RequestStateInterface } from './state';
import { StateInterface } from '../index';

const getters  : GetterTree<RequestStateInterface, StateInterface> = {
    getRequestsLocally(state) {
        return state.requests;
    },
    getPendingRequestsLocally(state) {
        return state.requests.filter(req => req.status === 'pending');
    }
}

export default getters;