export interface RequestInterface {
    requestId: string;
    status: 'pending' | 'approved' | 'rejected';
    amount: number;
    purpose: string;
    requester: string;
    repay_frequency: string;
}

export interface RequestStateInterface {
    requests: Array<RequestInterface>
}

const state: RequestStateInterface = {
    requests: []
}

export default state;