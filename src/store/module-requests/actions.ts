import { ActionTree } from 'vuex';
import { RequestStateInterface } from './state';
import { StateInterface } from '../index';

import { createRequest, updateRequest } from '../../firebase';

const actions: ActionTree<RequestStateInterface, StateInterface> = {
    setRequests(context, payload: RequestStateInterface) {
        context.commit('setRequests', payload);
    },
    async createARequest(context, payload: {amount: number, purpose: string}) {
        const success = await createRequest({
            status: 'pending',
            requester: 'u1',
            amount: payload.amount,
            purpose: payload.purpose,
            repay_frequency: 'weekly'
        });
        context.commit('createARequest', {...payload, requestId: success.id, status: 'pending', requester: 'u1', repay_frequency: 'weekly'});
    },
    async approveARequest(context, payload: {requestId: string, approvalStatus: 'pending' | 'approved' | 'rejected', amount: number}) {
        await updateRequest(payload.requestId, {status: payload.approvalStatus});
        context.commit('approveARequest', payload);
        if(payload.approvalStatus == 'approved') {
            void context.dispatch('transactions/createATransaction', {amount: payload.amount, type: 'credit'}, {root: true})
        }
    }
}

export default actions;