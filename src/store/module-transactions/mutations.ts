import { MutationTree } from 'vuex';
import { TransactionStateInterface, TransactionInterface } from './state';

const mutations : MutationTree<TransactionStateInterface> = {
    setTransactions(state, payload: TransactionStateInterface) {
        // careful with arrays
        state.transactions = [...payload.transactions];
    },
    createATransaction(state, payload: TransactionInterface) {
        state.transactions = [...state.transactions, payload ];
    }
}

export default mutations;