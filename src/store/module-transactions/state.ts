export interface TransactionInterface {
    transactionId: string;
    userId: string;
    amount: number;
    type: 'credit' | 'debit';
    date: string;
}

export interface TransactionStateInterface {
    transactions: Array<TransactionInterface>
}

const state: TransactionStateInterface = {
    transactions: []
}

export default state;