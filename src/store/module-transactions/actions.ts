import { ActionTree } from 'vuex';
import { TransactionStateInterface } from './state';
import { StateInterface } from '../index';

import { createTransaction } from '../../firebase';

const actions: ActionTree<TransactionStateInterface, StateInterface> = {
    setTransactions(context, payload: TransactionStateInterface) {
        context.commit('setTransactions', payload);
    },
    async createATransaction(context, payload: {amount: number, type: 'credit' | 'debit'}) {
        const success = await createTransaction({
            userId: 'u1',
            date: '25/07/2021',
            amount: payload.amount,
            type: payload.type
        });
        context.commit('createATransaction', {...payload, transactionId: success.id, userId: 'u1', date: '25/07/2021'});
        const transactionAmount = payload.type === 'credit' ? payload.amount : -Math.abs(payload.amount)
        void context.dispatch('users/updateUserBalance', transactionAmount, {root: true});
    }
}

export default actions;