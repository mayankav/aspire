import { GetterTree } from 'vuex';
import { TransactionStateInterface } from './state';
import { StateInterface } from '../index';

const getters  : GetterTree<TransactionStateInterface, StateInterface> = {
}

export default getters;