import state from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

import { Module } from 'vuex';
import { TransactionStateInterface } from './state';
import { StateInterface } from '../index';

const transactionsModule: Module<TransactionStateInterface,StateInterface> = {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}

export default transactionsModule;